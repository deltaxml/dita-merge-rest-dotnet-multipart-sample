﻿// Copyright (c) 2024 Deltaman group limited. All rights reserved.

using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.IO;
using System.Text;

namespace DotNetMultiPartSample
{
    class Program
    {
        private const string versionsName = "VersionOrder";
        private const string actionUrl = "http://localhost:8080/api/ditamerge/v1/types/concurrent";
        static void Main(string[] args)
        {
            try
            {
                // Read in Ancestor (A) and Version (B,C,D) files
                String inputA = File.ReadAllText(@"data/four-edits-anna.xml");
                String inputB = File.ReadAllText(@"data/four-edits-ben.xml");
                String inputC = File.ReadAllText(@"data/four-edits-chris.xml");
                String inputD = File.ReadAllText(@"data/four-edits-david.xml");

                Console.WriteLine("Calling concurrent merge REST API...");

                PostMultiPartContent(inputA, inputB, inputC, inputD).Wait();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"There was an exception: {ex.ToString()}");
            }
        }

        /// <summary>Build and POST a Multi-part form request.</summary>
        public static async Task PostMultiPartContent(String a, String b, String c, String d)
        {
            HttpContent textContentVersionOrder = new StringContent("anna,ben,chris,dave");
            HttpContent xmlContentA = new StringContent(a, Encoding.UTF8, "application/xml");
            HttpContent xmlContentB = new StringContent(b, Encoding.UTF8, "application/xml");
            HttpContent xmlContentC = new StringContent(c, Encoding.UTF8, "application/xml");
            HttpContent xmlContentD = new StringContent(d, Encoding.UTF8, "application/xml");

            using (HttpClient client = new HttpClient())
            using (MultipartFormDataContent formData = new MultipartFormDataContent())
            {
                formData.Add(textContentVersionOrder, versionsName);
                formData.Add(xmlContentA, "anna");
                formData.Add(xmlContentB, "ben");
                formData.Add(xmlContentC, "chris");
                formData.Add(xmlContentD, "dave");
                try
                {
                    HttpResponseMessage response = await client.PostAsync(actionUrl, formData);
                    Console.WriteLine(response.StatusCode.ToString());
                    if (!response.IsSuccessStatusCode)
                    {
                        Console.WriteLine("Error details from response:");
                        String errorDetails = response.Content.ReadAsStringAsync().Result;
                        Console.WriteLine(errorDetails);
                    }
                    else
                    {
                        String mergeResultString = response.Content.ReadAsStringAsync().Result;
                        // Write the content to the local file result.xml.
                        File.WriteAllText(@"data/result.xml", mergeResultString);
                    }
                }
                catch (Exception Error)
                {
                    Console.WriteLine(Error.Message);
                }
                finally
                {
                    client.CancelPendingRequests();
                }
            }
        }
    }
}
