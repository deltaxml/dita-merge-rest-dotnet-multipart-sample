# .NET Multi-Part Sample

Multipart form data post example for DITA Merge REST API from C#.

This is a C# sample of a way to call the DITA Merge REST API from C# and .NET.
This method uses a multipart/form-data MIME type POST, and adds each of the input files as a separate part.
It runs a concurrent merge on the ancestor input file, with name "four-edits-anna", and three version input files named "four-edits-ben", "four-edits-chris" and "four-edits-david".
The n-way concurrent merge type is specified by the post async URI of "api/ditamerge/v1/types/concurrent".
The merge result is written to a "result.xml" file in the data directory.
If there is a problem with the request, the error that is returned in the response is written to standard output.

This sample is deliberately kept simple, with minimal error handling, to concentrate on the useful information.

Minimum Requirements: .NET Core 2.0 or later Visual Studio Code (with extension: C# for Visual Studio Code) Operating System: Windows v7 or later, MacOS Sierra or later, Linux (see Visual Studio documentation for supported versions of Linux).
