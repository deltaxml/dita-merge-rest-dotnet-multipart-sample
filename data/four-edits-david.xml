<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE topic PUBLIC "-//OASIS//DTD DITA Topic//EN" "topic.dtd">
<topic id="topic_xxs_klq_fh">
  <title>Example for DeltaXML DITA Merge</title>
  <body>
    <p xml:lang="en">DeltaXML DITA Merge automatically merges edits from two or more editors into a single
      document, so you can accept/reject changes from multiple authors all in one document. With
      DeltaXML DITA Merge you are no longer restricted to having only one person working on a
      document.</p>
    <p>It is relatively easy to work on a document which is reviewed by one other person because you
      just need to compare two documents (or record changes) to see the changes. If there are two
      reviewers, then it becomes a lot more complicated, because you need to do two comparisons and
      then view three different documents (changes from each reviewer and your original) to produce
      a new version. For three or more reviewers, you soon run out of screen space to see what is
      going on.</p>
    <p>Now, with DeltaXML DITA Merge, this job becomes much, much easier, because DeltaXML DITA
      Merge merges all the changes into a single document.</p>
    <p>DeltaXML DITA Merge has a number of different use cases:</p>
    <ul>
      <li>
        <p>accept or reject changes from multiple reviewers by working on just one document</p>
      </li>
      <li>
        <p>allow more than one editor to work on a document simultaneously and merge their changes
          together</p>
      </li>
      <li>
        <p>allow intelligent resolution of conflicts in a Document Management System (DMS) when two
          users check in the same edited document</p>
      </li>
      <li>
        <p>merge changes from two different branches in a DMS</p>
      </li>
    </ul>
    <p>DeltaXML DITA Merge works at the level of each paragraph, table or list. DeltaXML DITA Merge
      compares each edited version with the base and if only one is different, the changes are shown
      as tracked changes so that they can be accepted or rejected.</p>
    <p>If more than one editor has changed the same item, the change from each editor is shown
      separately for easier understanding. At the same time, all the changes are intelligently
      merged into one copy of the paragraph or item to make it easier to accept or reject each
      change.</p>
    <p>DeltaXML DITA Merge is available as a Java API for integration into your existing DMS or
      other enterprise systems. Please contact us for further details.</p>
  </body>
</topic>
